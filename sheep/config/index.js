// 开发环境配置
import packageInfo from '@/package.json';

const { version } = packageInfo;
export let baseUrl;
if (process.env.NODE_ENV === 'development') {
  baseUrl = import.meta.env.SHOPRO_DEV_BASE_URL;
} else {
  baseUrl = import.meta.env.SHOPRO_BASE_URL;
}
if (typeof baseUrl === 'undefined') {
  console.error('请检查.env配置文件是否存在');
} else {
  console.log(`[Shopro ${version}]  https://www.sheepjs.com/`);
}
export const apiPath = import.meta.env.SHOPRO_API_PATH;

export const staticUrl = import.meta.env.SHOPRO_STATIC_URL;

export default {
  baseUrl,
  apiPath,
  staticUrl,
};
